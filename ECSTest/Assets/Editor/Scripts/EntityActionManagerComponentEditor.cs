﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using Assets.Editor.Scripts;
using ECS.ManagerComponents;
using ECS.Objects.Actions;

[CustomEditor(typeof(EntityActionManagerComponent))]
public class EntityActionManagerComponentEditor : Editor
{

    private bool _foldoutPossibleActions = true;
    private bool _foldoutVisibleActions = true;
    private bool _foldoutHiddenActions = true;
    private Vector2 _scrollPosition = new Vector2();

    public Dictionary<EntityAction, EntityActionDrawer> ActionToDrawer = new Dictionary<EntityAction, EntityActionDrawer>();


    private void DrawActions(EntityActionManagerComponent entityActionManager, List<EntityAction> actions)
    {
        foreach (var action in actions)
        {
            EntityActionDrawer drawer;
            if (!ActionToDrawer.TryGetValue(action, out drawer))
            {
                drawer = new EntityActionDrawer();
                ActionToDrawer.Add(action, drawer);
            }
            GUILayout.BeginHorizontal();
            var remove = (GUILayout.Button("x", GUILayout.Width(15f), GUILayout.Height(15f)));
            GUILayout.Space(20f);
            GUILayout.BeginVertical();
            drawer.DrawAction(action);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            if (remove)
            {
                entityActionManager.RemoveObject(action);
                ActionToDrawer.Remove(action);
            }
        }
    }

    private void DrawVisibleActions(EntityActionManagerComponent entityActionManager)
    {
        // Find all visible actions assigned to this Entity and display them
        _foldoutVisibleActions = EditorGUILayout.BeginFoldoutHeaderGroup(_foldoutVisibleActions, "Visible actions");

        if (_foldoutVisibleActions)
        {
            var actions = entityActionManager.GetVisibleActions();
            DrawActions(entityActionManager, actions);
        }

        EditorGUILayout.EndFoldoutHeaderGroup();
    }

    private void DrawHiddenActions(EntityActionManagerComponent entityActionManager)
    {
        // Find all hidden actions assigned to this Entity and display them
        _foldoutHiddenActions = EditorGUILayout.BeginFoldoutHeaderGroup(_foldoutHiddenActions, "Hidden actions");

        if (_foldoutHiddenActions)
        {
            var actions = entityActionManager.GetHiddenActions();
            DrawActions(entityActionManager, actions);
        }

        EditorGUILayout.EndFoldoutHeaderGroup();
    }


    private void DrawPossibleActions(EntityActionManagerComponent entityActionManager)
    {
        // Find all possible actions and display them
        _foldoutPossibleActions = EditorGUILayout.BeginFoldoutHeaderGroup(_foldoutPossibleActions, "Possible actions");
        if (_foldoutPossibleActions)
        {
            // Actions should be visible
            var possibleActions = AssetDatabase.FindAssets("t:entityaction");
            //var types = typeof(EntityAction).Assembly.GetTypes().Where(type => type.IsSubclassOf(typeof(EntityAction)));

            _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition, GUILayout.Height(Mathf.Min(25f * possibleActions.Count(), 200f)));
            foreach (var possibleAction in possibleActions)
            {
                var action = (EntityAction)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(possibleAction), typeof(EntityAction));
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Add", GUILayout.Height(15f)))
                {
                    entityActionManager.AddObject(action);
                    EditorUtility.SetDirty(action);
                    EditorUtility.SetDirty(entityActionManager);
                }
                EditorGUILayout.ObjectField(action, action.GetType(), allowSceneObjects: true, GUILayout.Height(15f));
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            }
            EditorGUILayout.EndScrollView();
        }
        EditorGUILayout.EndFoldoutHeaderGroup();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var entityActionManager = target as EntityActionManagerComponent;
        DrawVisibleActions(entityActionManager);
        DrawHiddenActions(entityActionManager);
        DrawPossibleActions(entityActionManager);
        
        foreach (var action in entityActionManager.GetObjects())
        {
            EditorUtility.SetDirty(action);
        }
    }

}
