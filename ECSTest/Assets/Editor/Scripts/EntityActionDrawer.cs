﻿using ECS.Objects.Actions;
using ECS.Objects.Data;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor.Scripts
{
    public class EntityActionDrawer
    {

        private bool _foldout = true;


        public void DrawAction(EntityAction action)
        {
            var fields = action.GetType().GetFields();

            _foldout = EditorGUILayout.Foldout(_foldout, action.GetType().Name, true);
            if (_foldout)
            {
                foreach (var field in fields)
                {
                    if (field.IsInitOnly)
                    {
                        EditorGUILayout.LabelField(field.GetValue(action).ToString());
                        continue;
                    }

                    if (field.FieldType.IsSubclassOf(typeof(UnityEngine.Object)))
                    {
                        object retVal;
                        if (field.GetValue(action) == null)
                            retVal = EditorGUILayout.ObjectField(field.Name, (UnityEngine.GameObject)null, field.FieldType, true);
                        else
                            retVal = EditorGUILayout.ObjectField(field.Name, (UnityEngine.Object)field.GetValue(action), field.FieldType, true);

                        field.SetValue(action, retVal);
                    }
                    else if (field.FieldType == typeof(float))
                    {
                        var retVal = EditorGUILayout.FloatField(field.Name, (float)field.GetValue(action));
                        field.SetValue(action, retVal);
                    }
                    else if (field.FieldType == typeof(int))
                    {
                        var retVal = EditorGUILayout.IntField(field.Name, (int)field.GetValue(action));
                        field.SetValue(action, retVal);
                    }
                    else if (field.FieldType == typeof(Vector3))
                    {
                        var retVal = EditorGUILayout.Vector3Field(field.Name, (Vector3)field.GetValue(action));
                        field.SetValue(action, retVal);
                    }
                    else if (field.FieldType == typeof(string))
                    {
                        var retVal = EditorGUILayout.TextField(field.Name, (string)field.GetValue(action));
                        field.SetValue(action, retVal);
                    }
                    else if (field.FieldType == typeof(KeyCode))
                    {
                        var retVal = (KeyCode)EditorGUILayout.EnumPopup(field.Name, (KeyCode)field.GetValue(action));
                        field.SetValue(action, retVal);
                    }
                    else if (field.FieldType == typeof(LayerMask))
                    {
                        List<string> names = new List<string>();
                        for (int i = 0; i < 32; ++i)
                        {
                            var name = LayerMask.LayerToName(i);
                            if (!string.IsNullOrEmpty(name))
                            {
                                names.Add(name);
                            }
                        }
                        var retVal = (LayerMask)EditorGUILayout.MaskField(field.Name, ((LayerMask)field.GetValue(action)).value, names.ToArray());
                        field.SetValue(action, retVal);
                    }
                    else if (field.FieldType == typeof(AnimationCurve))
                    {
                        var retVal = EditorGUILayout.CurveField(field.Name, (AnimationCurve)field.GetValue(action));
                        field.SetValue(action, retVal);
                    }
                    else if (field.FieldType == typeof(BlockingList))
                    {
                        EditorGUILayout.ObjectField(field.Name, (UnityEngine.Object)field.GetValue(action), field.FieldType, true);
                    }
                    else
                    {
                        Debug.LogWarning(field.FieldType + " not implemented in Drawer");
                    }
                }
            }
        }

    }
}
