﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Assets.Editor.Scripts;
using ECS.ManagerComponents;
using ECS.Objects.Data;
using ECS.Objects.Actions;

[CustomEditor(typeof(DataObjectManagerComponent))]
public class DataObjectManagerComponentEditorEditor : Editor
{

    private bool _foldoutExposedObjects = true;
    public PairData ExposedData;

    public Dictionary<EntityAction, EntityActionDrawer> ActionToDrawer = new Dictionary<EntityAction, EntityActionDrawer>();


    private void DrawExposedObjects(DataObjectManagerComponent dataObjectManager)
    {
        // Find all exposed objects by this Entity and display them
        _foldoutExposedObjects = EditorGUILayout.BeginFoldoutHeaderGroup(_foldoutExposedObjects, "Exposed objects");

        if (dataObjectManager != null)
        {
            if (_foldoutExposedObjects)
            {
                var dataPairs = dataObjectManager.GetExposedVariables();

                foreach (var dataPair in dataPairs)
                {
                    if (dataPair != null)
                    {
                        // Draw the pairs with button to remove it
                        GUILayout.BeginHorizontal();
                        var remove = (GUILayout.Button("x", GUILayout.Width(15f), GUILayout.Height(15f)));
                        GUILayout.Space(20f);
                        dataPair.Object = (GameObject)EditorGUILayout.ObjectField(dataPair.Name, dataPair.Object, typeof(GameObject), true);
                        GUILayout.EndHorizontal();

                        if (remove)
                        {
                            dataObjectManager.RemoveObject(dataPair);
                        }
                    }
                }
            }
        }

        GUILayout.BeginHorizontal();
        ExposedData = (PairData)EditorGUILayout.ObjectField(ExposedData, typeof(PairData), true);
        bool add = GUILayout.Button("Expose object", GUILayout.Height(15f));
        GUILayout.EndHorizontal();
        if (add)
        {
            if (dataObjectManager.Exists(ExposedData.Name))
            {
                Debug.LogWarning("Pair with name '" + ExposedData.Name + "' already exists.");
            }
            else
            {
                dataObjectManager.AddObject(ExposedData);
                EditorUtility.SetDirty(dataObjectManager);
            }
        }

        EditorGUILayout.EndFoldoutHeaderGroup();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var dataObjectManager = target as DataObjectManagerComponent;
        DrawExposedObjects(dataObjectManager);
    }

}
