﻿using ECS.Objects;
using UnityEngine;

namespace ECS
{

    public class Game : MonoBehaviour
    {

        public FloatBinding GameTime;
        public FloatBinding TotalScore;


        // Update is called once per frame
        void Update()
        {
            GameTime.Value -= Time.deltaTime;

            if (GameTime.Value <= 0f)
                Time.timeScale = 0f;
        }

        public void CutTime(float timeRemoved)
        {
            GameTime.Value -= timeRemoved;
        }

        public void IncreaseScore(float reward)
        {
            TotalScore.Value += reward;
        }
    }

}