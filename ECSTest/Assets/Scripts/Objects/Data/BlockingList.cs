﻿using ECS.Objects.Actions;
using System.Collections.Generic;
using UnityEngine;


namespace ECS.Objects.Data
{

    [CreateAssetMenu(fileName = "New Block List", menuName = "Entity/Objects/Create Blocking List")]
    public class BlockingList : Object
    {

        public List<EntityAction> BlockedActionList;

    }

}