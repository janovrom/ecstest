﻿using System;
using UnityEngine;


namespace ECS.Objects.Data
{


    [Serializable]
    [CreateAssetMenu(fileName = "New Shared Data", menuName = "Entity/Objects/Create Shared Data")]
    public class PairData : Object
    {

        public string Name;
        public GameObject Object;


        public PairData(string key, GameObject data)
        {
            Name = key;
            Object = data;
        }

        public override bool Equals(object obj)
        {
            return obj is PairData data &&
                   Name == data.Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }


    }
}