﻿using ECS.Events;
using System;
using UnityEngine;


namespace ECS.Objects
{

    [CreateAssetMenu(fileName ="New Float Binding", menuName ="Float Binding")]
    [Serializable]
    public class FloatBinding : ScriptableObject
    {

        [SerializeField]
        private float _value;

        private float _runtimeValue;

        public float Value
        {
            get
            {
                return _runtimeValue;
            }

            set
            {
                _runtimeValue = value;
                ValueChangedEvent.Invoke(_runtimeValue);
            }
        }

        public FloatGameEvent ValueChangedEvent;


        private void OnEnable()
        {
            _runtimeValue = _value;
        }

    }
}
