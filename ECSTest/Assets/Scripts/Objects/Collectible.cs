﻿using ECS.Events;
using UnityEngine;


namespace ECS.Objects
{

    public class Collectible : MonoBehaviour
    {
        public float Value = 10f;
        public FloatGameEvent OnPickUp;


        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                OnPickUp.Invoke(Value);
                Destroy(this.gameObject);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                OnPickUp.Invoke(Value);
                Destroy(this.gameObject);
            }
        }
    }

}
