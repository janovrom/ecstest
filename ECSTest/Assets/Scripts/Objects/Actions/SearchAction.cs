﻿using ECS.ManagerComponents;
using ECS.Objects.Data;
using UnityEngine;


namespace ECS.Objects.Actions
{

    [CreateAssetMenu(fileName = "New Search Action", menuName = "Entity/EntityAction/Create Search Action")]
    public class SearchAction : EntityAction
    {

        public string Target = "SearchTarget";
        public readonly string ExposesTarget = "FollowTarget";
        public float SearchRadius = 2f;


        public SearchAction() : base(true)
        {
        }

        public override void Execute(Entity entity)
        {
            // Search
            var dataManager = entity.GetManagerComponent<DataObjectManagerComponent>();

            var searchForData = (PairData)dataManager.GetObject(Target);
            if (searchForData == null)
            {
                return;
            }

            if (searchForData.Object == null)
            {
                searchForData.Object = GameObject.FindWithTag(Target);
            }

            var target = searchForData.Object;

            var moveDirection = target.transform.position - entity.transform.position;
            if (moveDirection.magnitude < SearchRadius)
            {
                var data = ScriptableObject.CreateInstance<PairData>();
                data.Name = ExposesTarget;
                data.Object = target;
                dataManager.AddObject(data);
            }
            else
            {
                dataManager.Remove(ExposesTarget);
            }
        }

    }

}