﻿using System.Collections;
using UnityEngine;

namespace ECS.Objects.Actions
{

    [CreateAssetMenu(fileName = "New Input Jump Action", menuName = "Entity/EntityAction/Create Input Jump Action")]
    public class InputJumpAction : EntityAction
    {

        public float JumpLength = 2f;
        public float JumpTime = 2f;
        public AnimationCurve JumpCurve;
        public KeyCode JumpKey = KeyCode.Space;

        private bool _isJumping = false;
        private Entity _entity;


        public InputJumpAction() : base(false)
        {
        }

        private IEnumerator Jump()
        {
            var time = 0f;
            var initVerticalPos = _entity.transform.position.y;
            var initPos = _entity.transform.position;
            var forward = _entity.transform.forward;
            while (time <= JumpTime)
            {
                yield return new WaitForFixedUpdate();
                time += Time.fixedDeltaTime;
                var verticalPos = JumpCurve.Evaluate(time);
                _entity.transform.position = initPos + Vector3.up * verticalPos + forward * time * JumpLength / JumpTime;
            }

            _entity.transform.position = new Vector3(_entity.transform.position.x, initVerticalPos, _entity.transform.position.z);
            _isJumping = false;
            _entity = null;
        }

        public override void Execute(Entity entity)
        {
            if (!_isJumping && Input.GetKey(JumpKey))
            {
                _isJumping = true;
                _entity = entity;
                entity.StartCoroutine(Jump());
            }
        }

    }

}