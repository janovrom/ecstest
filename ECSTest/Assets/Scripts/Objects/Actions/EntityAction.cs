﻿using ECS.Objects.Data;

namespace ECS.Objects.Actions
{

    public abstract class EntityAction : Object
    {

        public bool CanBeVisible { get; }

        public bool IsShared = false;
        public BlockingList BlockedActions;



        public EntityAction(bool isVisible)
        {
            CanBeVisible = isVisible;
        }

        public abstract void Execute(Entity entity);

#if UNITY_EDITOR
        private void OnValidate()
        {
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif

    }

}