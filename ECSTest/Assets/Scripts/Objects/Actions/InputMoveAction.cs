﻿using UnityEngine;

namespace ECS.Objects.Actions
{

    [CreateAssetMenu(fileName = "New Input Move Action", menuName = "Entity/EntityAction/Create Input Move Action")]
    public class InputMoveAction : EntityAction
    {

        public float Speed = 2f;


        public InputMoveAction() : base(false)
        {
        }

        public override void Execute(Entity entity)
        {
            var horizontalInput = Input.GetAxis("Horizontal");
            var verticalInput = Input.GetAxis("Vertical");

            var hSign = 0f;
            var vSign = 0f;

            if (horizontalInput > 0.00001f) hSign = 1f;
            else if (horizontalInput < -0.00001f) hSign = -1f;

            if (verticalInput > 0.00001f) vSign = 1f;
            else if (verticalInput < -0.00001f) vSign = -1f;

            // Face direction based on input sign
            var hMoveDirection = Vector3.right * hSign;
            var vMoveDirection = Vector3.forward * vSign;

            var moveDirection = hMoveDirection + vMoveDirection;
            moveDirection.Normalize();

            if (moveDirection.magnitude > 0.0000f)
            {
                entity.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
                entity.transform.rotation = Quaternion.LookRotation(moveDirection);
            }
        }

    }

}