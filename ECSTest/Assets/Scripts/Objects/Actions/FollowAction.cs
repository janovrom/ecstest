﻿using ECS.ManagerComponents;
using UnityEngine;

namespace ECS.Objects.Actions
{

    [CreateAssetMenu(fileName = "New Follow Action", menuName = "Entity/EntityAction/Create Follow Action")]
    public class FollowAction : EntityAction
    {

        public readonly string Target = "FollowTarget";
        public float Speed = 2f;


        public FollowAction() : base(true)
        {
        }

        public override void Execute(Entity entity)
        {
            // Follow
            var dataManager = entity.GetManagerComponent<DataObjectManagerComponent>();

            var target = (GameObject)dataManager.GetObject(Target)?.Object;
            if (target == null)
            {
                return;
            }

            var moveDirection = target.transform.position - entity.transform.position;
            moveDirection.y = 0;
            moveDirection.Normalize();

            if (moveDirection.magnitude > 0.00001f)
            {
                entity.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
                entity.transform.rotation = Quaternion.LookRotation(moveDirection);
            }
        }

    }

}