﻿using ECS.ManagerComponents;
using UnityEngine;

namespace ECS.Objects.Actions
{

    [CreateAssetMenu(fileName = "New Wander Action", menuName = "Entity/EntityAction/Create Wander Action")]
    public class WanderAction : EntityAction
    {

        public LayerMask WalkableLayer;
        public float Speed = 2f;
        [Range(0f, 1f)]
        public float DirectionChangeProbability = 0.5f;
        public readonly string DoesNotHave = "FollowTarget";

        private Vector3 _moveDirection = Vector3.zero;


        public WanderAction() : base(true)
        {
        }

        public override void Execute(Entity entity)
        {
            var dataManager = entity.GetManagerComponent<DataObjectManagerComponent>();

            var target = dataManager.GetObject(DoesNotHave)?.Object;
            if (target != null)
            {
                return;
            }

            if (Random.Range(0f, 1f) < DirectionChangeProbability)
            {
                _moveDirection = Vector3.Slerp(_moveDirection, Random.onUnitSphere, 0.1f);
                _moveDirection.y = 0f;
                _moveDirection.Normalize();
            }

            if (Physics.Raycast(entity.transform.position + _moveDirection + Vector3.up, Vector3.down, WalkableLayer))
            {
                entity.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
                entity.transform.rotation = Quaternion.LookRotation(_moveDirection);
            }
        }

    }

}