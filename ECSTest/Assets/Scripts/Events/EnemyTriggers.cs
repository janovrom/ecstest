﻿using UnityEngine;


namespace ECS.Events
{

    public class EnemyTriggers : MonoBehaviour
    {

        public FloatGameEvent OnPlayerReached;
        public float Penalty = 5f;


        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                OnPlayerReached.Invoke(Penalty);
                Destroy(this.gameObject);
            }
        }

    }

}