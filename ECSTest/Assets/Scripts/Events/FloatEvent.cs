﻿using UnityEngine.Events;

namespace ECS.Events
{

    [System.Serializable]
    public class FloatEvent : UnityEvent<float>
    {
    }

}