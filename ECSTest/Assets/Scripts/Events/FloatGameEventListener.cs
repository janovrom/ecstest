﻿using UnityEngine;


namespace ECS.Events
{

    public class FloatGameEventListener : MonoBehaviour
    {

        public FloatGameEvent gameEvent;
        public FloatEvent actionTaken;


        private void OnEnable()
        {
            gameEvent.AddListener(this);
        }

        private void OnDisable()
        {
            gameEvent.RemoveListener(this);
        }

        public void OnEventRaised(float arg)
        {
            actionTaken.Invoke(arg);
        }

    }

}
