﻿using System.Collections.Generic;
using UnityEngine;


namespace ECS.Events
{

    [CreateAssetMenu(fileName ="New Game Event", menuName ="Game Event")]
    public class FloatGameEvent : ScriptableObject
    {

        [SerializeField]
        private List<FloatGameEventListener> listeners = new List<FloatGameEventListener>();


        public void AddListener(FloatGameEventListener listener)
        {
            listeners.Add(listener);
        }

        public void RemoveListener(FloatGameEventListener listener)
        {
            listeners.Remove(listener);
        }

        public void Invoke(float arg)
        {
            for (int i = listeners.Count - 1; i >= 0; --i)
            {
                listeners[i].OnEventRaised(arg);
            }
        }
    }

}