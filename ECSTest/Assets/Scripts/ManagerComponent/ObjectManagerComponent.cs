﻿using System.Collections.Generic;

namespace ECS.ManagerComponents
{

    public abstract class ObjectManagerComponent<T> : ManagerComponent where T : Objects.Object
    {

        public abstract ICollection<T> GetObjects();

        public abstract void RemoveObject(T o);

        public abstract bool AddObject(T o);

        public abstract void Clear();

    }
}