﻿using ECS.Objects.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace ECS.ManagerComponents
{


    [Serializable]
    public class IntPairDictionary : Dictionary<int, PairData>, ISerializationCallbackReceiver
    {
        [SerializeField]
        private List<int> keys = new List<int>();
        [SerializeField]
        private List<PairData> values = new List<PairData>();


        public void OnBeforeSerialize()
        {
            keys.Clear();
            values.Clear();
            foreach (KeyValuePair<int, PairData> pair in this)
            {
                keys.Add(pair.Key);
                values.Add(pair.Value);
            }
        }

        public void OnAfterDeserialize()
        {
            this.Clear();
            for (int i = 0; i < keys.Count; i++)
                this.Add(keys[i], values[i]);
        }
    }

    public class DataObjectManagerComponent : ObjectManagerComponent<PairData>
    {

        [SerializeField]
        private IntPairDictionary _objects = new IntPairDictionary();


        public override bool AddObject(PairData o)
        {
            int key = o.GetHashCode();
            if (_objects.ContainsKey(key))
                return false;

            _objects.Add(key, o);
            return true;
        }

        public bool Exists(string name)
        {
            int key = name.GetHashCode();
            return _objects.ContainsKey(key);
        }

        public List<PairData> GetExposedVariables()
        {
            return _objects.Values.ToList();
        }

        public PairData GetObject(string keyString)
        {
            var key = keyString.GetHashCode();
            PairData ret;
            var l = _objects.Values.ToList();
            if (_objects.TryGetValue(key, out ret))
            {
                return ret;
            }

            return null;
        }

        public override ICollection<PairData> GetObjects()
        {
            return _objects.Values.ToList();
        }


        public bool Remove(string name)
        {
            int key = name.GetHashCode();
            return _objects.Remove(key);
        }

        public override void RemoveObject(PairData o)
        {
            _objects.Remove(o.GetHashCode());
        }

        public override void Clear()
        {
            _objects.Clear();
        }

    }
}