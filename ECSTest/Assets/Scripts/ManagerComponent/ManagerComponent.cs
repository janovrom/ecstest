﻿using UnityEngine;

namespace ECS.ManagerComponents
{

    [RequireComponent(typeof(Entity))]
    [ExecuteInEditMode]
    public abstract class ManagerComponent : MonoBehaviour
    {

        private void OnEnable()
        {
            SendMessageUpwards("OnManagerComponentAttached", this, SendMessageOptions.DontRequireReceiver);
        }

        private void OnDisable()
        {
            SendMessageUpwards("OnManagerComponentDetached", this, SendMessageOptions.DontRequireReceiver);
        }

    }
}
