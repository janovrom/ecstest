﻿using System.Collections;
using UnityEngine;

namespace ECS.ManagerComponents
{

    public class SpawnManager : MonoBehaviour
    {

        public BoxCollider SpawnArea;
        public GameObject prefab;
        public Transform SpawnParent;
        public float RespawnTime = 10.0f;


        IEnumerator Start()
        {
            // TODO Game ended
            while (true)
            {
                float x = Random.Range(SpawnArea.bounds.min.x, SpawnArea.bounds.max.x);
                float z = Random.Range(SpawnArea.bounds.min.z, SpawnArea.bounds.max.z);
                var spawn = Instantiate(prefab, SpawnParent);
                spawn.transform.localPosition = new Vector3(x, 0, z);

                yield return new WaitForSeconds(RespawnTime);
            }
        }

    }

}