﻿using ECS.Objects.Actions;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ECS.ManagerComponents
{

    public class EntityActionManagerComponent : ObjectManagerComponent<EntityAction>
    {

        [HideInInspector]
        [SerializeField]
        private List<EntityAction> _objects = new List<EntityAction>();


        private void Awake()
        {
#if !UNITY_EDITOR
            // Take all the actions that are not shared and make copy of it
            var tmpList = _objects;
            _objects = new List<EntityAction>();

            foreach (var action in tmpList)
            {
                if (action.IsShared)
                {
                    _objects.Add(action);
                }
                else
                {
                    var newAction = Instantiate(action);
                    _objects.Add(newAction);
                }
            }
#endif
        }

        public List<EntityAction> GetVisibleActions()
        {
            return (_objects as List<EntityAction>).Where((entityAction) => entityAction.CanBeVisible).ToList();
        }

        public List<EntityAction> GetHiddenActions()
        {
            return (_objects as List<EntityAction>).Where((entityAction) => !entityAction.CanBeVisible).ToList();
        }

        internal void ExecuteActions(Entity entity)
        {
            var count = (_objects as List<EntityAction>).Count;
            for (int i = 0; i < count; ++i)
            {
                (_objects[i] as EntityAction).Execute(entity);
            }
        }

        public void AddObjectRange(IEnumerable<EntityAction> o)
        {
            (_objects as List<EntityAction>).AddRange(o);
        }

        public override ICollection<EntityAction> GetObjects()
        {
            return _objects;
        }

        public override void RemoveObject(EntityAction o)
        {
            _objects.Remove(o);
        }

        public override bool AddObject(EntityAction o)
        {
            _objects.Add(o);
            return true;
        }

        public override void Clear()
        {
            _objects.Clear();
        }

    }
}