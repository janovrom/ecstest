﻿using ECS.ManagerComponents;
using ECS.Objects.Actions;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace ECS
{

    [ExecuteInEditMode]
    public class Entity : MonoBehaviour
    {

        private List<ManagerComponent> _components { get; set; } = new List<ManagerComponent>();


        private void Start()
        {
            // Topo sort actions
            TopologicallySortActions();
        }

        private void Visit(EntityAction action, ref List<EntityAction> marks, ref List<EntityAction> tempMarks)
        {
            if (marks.Contains(action))
                return;

            if (tempMarks.Contains(action))
            {
                Debug.LogError("Actions have cyclic reference!");
                return;
            }

            tempMarks.Insert(0, action);
            if (action.BlockedActions != null)
            {
                foreach (var blockedAction in action.BlockedActions.BlockedActionList)
                {
                    Visit(blockedAction, ref marks, ref tempMarks);
                }
            }
            tempMarks.Remove(action);
            marks.Insert(0, action);
        }

        private void TopologicallySortActions()
        {
            var actionManagerComponent = GetManagerComponent<EntityActionManagerComponent>();
            if (actionManagerComponent)
            {
                List<EntityAction> marks = new List<EntityAction>();
                List<EntityAction> tempMarks = new List<EntityAction>();
                var actions = actionManagerComponent.GetObjects().ToList();

                while (actions.Count > 0)
                {
                    var action = actions[0];
                    actions.RemoveAt(0);

                    Visit(action, ref marks, ref tempMarks);
                }

                actionManagerComponent.Clear();
                actionManagerComponent.AddObjectRange(marks);
            }
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                var actionManagerComponent = GetManagerComponent<EntityActionManagerComponent>();
                actionManagerComponent?.ExecuteActions(this);
            }
#else
        var actionManagerComponent = GetManagerComponent<EntityActionManagerComponent>();
        actionManagerComponent?.ExecuteActions(this);
#endif
        }

        public void Reset()
        {
            var components = GetComponentsInChildren<ManagerComponent>();
            _components.AddRange(components);
        }

        public void OnManagerComponentAttached(ManagerComponent managerComponent)
        {
            if (!_components.Contains(managerComponent))
                _components.Add(managerComponent);
        }

        public void OnManagerComponentDetached(ManagerComponent managerComponent)
        {
            _components.Remove(managerComponent);
        }

        public T GetManagerComponent<T>() where T : ManagerComponent
        {
            foreach (var c in _components)
            {
                if (c is T)
                {
                    return (T)c;
                }
            }
            return default;
        }

        public T AddManagerComponent<T>() where T : ManagerComponent
        {
            var t = gameObject.AddComponent<T>();
            OnManagerComponentAttached(t);
            return t;
        }

    }

}