﻿using UnityEngine;
using UnityEngine.UI;


namespace ECS
{

    public class FloatToText : MonoBehaviour
    {

        public Text text;


        public void SetText(float value)
        {
            text.text = ((int)value).ToString();
        }

    }

}