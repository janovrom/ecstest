## Time spent
Times and logs are available at https://gitlab.com/janovrom/ecstest/issues?scope=all&utf8=%E2%9C%93&state=all in the form of issues and its assigned commits.

### Initial solution
* Large amount of time took deciphering the task. When you say ECS I imagine something like [this](https://tsprojectsblog.files.wordpress.com/2017/11/ecs_overview.png), which did not make sense with specified sub-tasks. Just to be sure I tried to find if there is anything similar. __4h__ (this is the right time to ask the person creating the task)
* Creating a simple mini-game for later conversion into the specified structure. __2h__ issue #1
* Converting the logic into EntityActions and creating the EntityActionManagerComponent. __1h__ issue #2
* Creating structure for storing objects - DataObject extends Object. __1h__ issuer #3
* Creating Entity class (extends MonoBehavior) and give it functions to work with ManagerComponent. ManagerComponent provides common interface for ObjectManagerComponent and its subclasses (workaround for storing different manager components into one list). Created an inspector for EntityActionManagerComponent that shows visible actions and allows to add another action. __2h__ issue #4
* Creating Search and Follow actions for the enemy. __1.5h__ issue #5
* Persist the actions and components: recompile, entering play mode, both removes all non-serialized objects and references. __10h__ issues #6, #7, #8, #9
* Converting rest of the actions. __2.5h__ issue #10

The work until now had one issue: serialization when building. All the created references to objects in the scene are lost during build. It can be avoided only for the editor using editor function InstanceToID(). The references are stored as an game object id and when reloaded, the lost GameObject can be recovered using its id. This is not possible during runtime as the editor function does not exist. It could be resolved by searching using names, but that's hardly a good solution.
This version can be found under this [tag](https://gitlab.com/janovrom/ecstest/-/tags/Custom-version).

### Converting into Unity structure
* Converting into MonoBehavior for components and ScriptableObject for data and actions. Adding messages when a ManagerComponent is added so it can be caught by Entity and the component is assigned to it automatically. Reseting the Entity also finds all ManagerComponents in the hierarchy. __1.5h__ issue #11
* Adding structure to actions: action can now block other actions that are then topologically sorted so that the blocked actions are called after the blocking actions. __1h__ issue #12
* Creating Events and DataBinding (=an event with value that reports changes). __1.5h__ issue #13
* Creating class and sequence diagrams, testing the build.__2h__
This project is stored under the second [tag](https://gitlab.com/janovrom/ecstest/-/tags/final-version). The build can be downloaded [here](https://gitlab.com/janovrom/ecstest/blob/master/ECSTest.zip). The uml diagrams are generated using plant-uml and its markdown files can be found [here](https://gitlab.com/janovrom/ecstest/tree/master/Doc).


The overall time spent on this task:
__30h__

## Solution description
I provide three class diagrams that describe events, objects, and the ManagerComponent. There is also one sequence diagram that show how the events are propagated.

### Events
The project needed only events that send float as its payload thus there is only a non-generic FloatGameEvent that extends ScriptableObject. Being non-generic, it can be created 
using CreateAssetMenu attribute (creates new menu item under Assets->Create). When created as an asset it can be added to MonoBehavior FloatGameEventListener that registers
itself to this asset event. FloatGameEventListener has FloatEvent which can reference objects in scene and call its methods.

### Manager Components
There is an abstract class ManagerComponent that extends MonoBehavior. When added to an game object in the scene it creates new Entity or assign itself to existing one.
ManagerComponent provides two methods OnEnable and OnDisable (from MonoBehavior) that send messages up the hierarchy to inform entities that the were attached/detached.
It also provides common interface for generic ObjectManagerComponent so it can be stored in a single list. The implementations by itself don't provide too much functionality
except exposing the stored data/actions - DataObjectManagerComponent and EntityActionManagerComponent.

### Actions and Objects
Both are subclasses of ScriptableObject and the actions can be created as an asset. Actions can be marked as shared so when the game is run the action with its 
instance properties is shared. When marked as not-shared, the EntityActionManagerComponent creates new instances of the actions and topologically sorts them. There 
are five actions and two entities - Player (InputMoveAction, InputJumpAction), Enemy (WanderAction, SearchAction, FollowAction). The search action blocks the follow
action, because it exposes found player (as data referenced by string - PairData) that is used by FollowAction. Basically, the enemy wanders around and if the player is 
found in a radius it follows him unless he/she runs away.

### Outside the specs
There are MonoBehaviors EnemyTriggers, Game, Collectible, and SpawnManager that mostly handle events and spawning of objects. EntityActionManagerComponent and 
DataObjectManagerComponent, each has its own editor in which objects/actions can be viewed/added.

### Limitations and Obstacles
The first limitations is the serialization and references which resulted in structure using unity objects - MonoBehavior and ScriptableObject. The biggest 
obstacle was the missing use case (who will use it, how will it be used, etc.). If known, the manager components exposed in Inspector could provide more useful
information where it now only shows visible, hidden and all actions in project under 3 different foldouts. One shortcoming is definitely the duplication of data - the actions are assets in the project and also part of manager component but it can be edited
in both. There is also quite nice way to build actions in Unity (state machines or behavior trees) using the ScriptableObjects and Animator. 
There should also be better way to represent data if not trying to fit the requirements. If the actions would be unchanged during run-time, it might benefit 
from generating custom class for each EntityActionManagerComponent.

The biggest obstacle was the too-abstract task, the missing use-case, and that it completely matches GameObject-MonoBehavior structure in Unity and does not 
correspond to the Entity-Component-System (which is normally the time when you ask the person reporting the issue for more information). Otherwise it was 
quite straightforward implementation except all those cases in serialization.


<object data="https://gitlab.com/janovrom/ecstest/blob/master/Doc/events_class_diagram.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="https://gitlab.com/janovrom/ecstest/blob/master/Doc/events_class_diagram.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="https://gitlab.com/janovrom/ecstest/blob/master/Doc/events_class_diagram.pdf">Events class diagram</a>.</p>
    </embed>
</object>
<object data="https://gitlab.com/janovrom/ecstest/blob/master/Doc/managercomponents_class_diagram.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="https://gitlab.com/janovrom/ecstest/blob/master/Doc/managercomponents_class_diagram.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="https://gitlab.com/janovrom/ecstest/blob/master/Doc/managercomponents_class_diagram.pdf">Manager component class diagram</a>.</p>
    </embed>
</object>
<object data="https://gitlab.com/janovrom/ecstest/blob/master/Doc/objects_class_diagram.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="https://gitlab.com/janovrom/ecstest/blob/master/Doc/objects_class_diagram.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="https://gitlab.com/janovrom/ecstest/blob/master/Doc/objects_class_diagram.pdf">Objects class diagram</a>.</p>
    </embed>
</object>
<object data="https://gitlab.com/janovrom/ecstest/blob/master/Doc/events.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="https://gitlab.com/janovrom/ecstest/blob/master/Doc/events.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="https://gitlab.com/janovrom/ecstest/blob/master/Doc/events.pdf">Events sequence diagram</a>.</p>
    </embed>
</object>


