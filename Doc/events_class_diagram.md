@startuml

namespace UnityEngine {

    class ScriptableObject {

    }

    class MonoBehavior {

    }

    class UnityEvent {

    }

}

namespace ECS.Events {

    class EnemyTriggers {
        +OnPlayerReached: FloatGameEvent
        +Penalty: float
    }

    class FloatEvent <float> {
    }

    class FloatGameEvent {
    }

    class FloatGameEventListener {
    }

    FloatGameEventListener "*" -- "1" FloatGameEvent : responds to >
    FloatGameEventListener "*" -- "1" FloatEvent : calls >

}

ECS.Events.FloatGameEvent --|> UnityEngine.ScriptableObject
ECS.Events.FloatGameEventListener --|> UnityEngine.MonoBehavior
ECS.Events.EnemyTriggers --|> UnityEngine.MonoBehavior
ECS.Events.FloatEvent --|> UnityEngine.UnityEvent
@enduml