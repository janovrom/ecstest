@startuml

namespace UnityEngine {

    class ScriptableObject {

    }

    class MonoBehavior {

    }

}

namespace ECS {
    class Game {

    }

    Game -left-|> UnityEngine.MonoBehavior
    Game "1" *-- "2" ECS.Objects.FloatBinding : time and score >
}

namespace ECS.Objects {

    namespace Data {
        class BlockingList {

        }

        class PairData {
            +Name: string
            +Object: GameObject
        }
    }

    namespace Actions {
        class EntityAction {
            +readonly CanBeVisible: bool
            +BlockedActions: BlockingList
            +abstract Execute(Entity): void
        }

        class FollowAction {
            +readonly Target: string
            +Speed: float
        }

        class InputJumpAction {
            +JumpLength: float
            +JumpTime: float
            +JumpCurve: AnimationCurve
            +JumpKey: KeyCode
        }

        class InputMoveAction {
            +Speed: float
        }

        class SearchAction {
            +Target: string
            +readonly ExposesTarget: string
            +SearchRadius: float
        }

        class WanderAction {
            +WalkableLayer: LayerMask
            +readonly DoesNotHave: string
            +Speed: float
            +[Range(0,1)] DirectionChangeProbability: float
        }

        WanderAction -up-|> EntityAction
        SearchAction -up-|> EntityAction
        InputMoveAction -up-|> EntityAction
        InputJumpAction -up-|> EntityAction
        FollowAction -up-|> EntityAction
    }

    ECS.Objects.Actions.EntityAction "*" -- "*" ECS.Objects.Data.BlockingList : blocks actions >

    class Collectible {
        +Value: float
        +OnPickUp: FloatGameEvent
        -OnTriggerEnter(Collider): void
    }

    class FloatBinding {
        -_value: [SerializeField] float
        -_runtimeValue: float
        +ValueChangedEvent: FloatGameEvent
    }

    class Object {
    }

    ECS.Objects.Object -up-|> UnityEngine.ScriptableObject
    ECS.Objects.FloatBinding -up-|> UnityEngine.ScriptableObject
    ECS.Objects.Collectible -up-|> UnityEngine.MonoBehavior

    ECS.Objects.Actions.EntityAction -up-|> ECS.Objects.Object
    ECS.Objects.Data.BlockingList -up-|> ECS.Objects.Object
    ECS.Objects.Data.PairData -up-|> ECS.Objects.Object
}

@enduml