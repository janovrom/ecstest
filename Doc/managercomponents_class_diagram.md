@startuml

namespace UnityEngine {

    class MonoBehavior {

    }

    class ScriptableObject {

    }

    ScriptableObject -[hidden] MonoBehavior

}

namespace ECS.ManagerComponents {

    class DataObjectManagerComponent <PairData> {
    }

    class EntityActionManagerComponent <EntityAction> {
    }

    abstract class ManagerComponent {
        -OnEnable(): void
        -OnDisable(): void
    }

    abstract class ObjectManagerComponent <T extends ECS.Objects.Object> {
        +abstract GetObjects(): ICollection<T>
        +abstract RemoveObject(T o): void
        +abstract AddObject(T o): bool
        +abstract Clear(): void
    }

    ObjectManagerComponent --|> ManagerComponent
    ManagerComponent --|> UnityEngine.MonoBehavior
    EntityActionManagerComponent --|> ObjectManagerComponent
    DataObjectManagerComponent --|> ObjectManagerComponent

    ObjectManagerComponent "*" -- "*" ECS.Objects.Object : has >

}

namespace ECS.Objects {
    class Object {
    }

    Object -left-|> UnityEngine.ScriptableObject
}

namespace ECS {
    class Entity {
        -TopologicallySortActions(): void
        -Update: void
        -Reset: void
        +OnManagerComponentAttached(ManagerComponent): void
        +OnManagerComponentDetached(ManagerComponent): void
        +AddManagerComponent<T>(): T
        +GetManagerComponent<T>(): T
    }

    note top of Entity
        Receives messages from ManagerComponent for attach and detach sent 
        from OnEnable and OnDisable. Attaches/Detaches the component from
        its list into Entity up in the hierarchy. On reset, Entity finds 
        all ManagerComponents under its hierarchy.

        Each update executes all actions that are topologically sorted
        based on the blocking lists.
    end note

    Entity -right-|> UnityEngine.MonoBehavior
    Entity "1" -- "*" ECS.ManagerComponents.ManagerComponent
}

@enduml