@startuml

title Sequence of event propagation using custom FloatGameEvent and FloatBinding

entity EnemyTriggers
entity PlayerReachedEvent
entity Game
entity TimeBinding
boundary Time


[-> EnemyTriggers : OnCollisionEnter
EnemyTriggers -> PlayerReachedEvent
EnemyTriggers -> EnemyTriggers : Destroy(gameObject)
PlayerReachedEvent -> Game : Call listeners
Game -> Game : Update time (float) binding
[-> TimeBinding : Value changed
TimeBinding -> Time : Call listeners
Time -> Time : Update text

@enduml